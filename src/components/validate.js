export const Validate = (data,type) => {
    
    const errors = {};

    
    if (!data.email) {
        errors.email = "Email required"
    } else if (!/\S+@\S+\.\S+/.test(data.email)) {
        errors.email = "Email address is invalid"
    } else {
        delete errors.email
    }

    if (!data.password) {
        errors.password = "username requird"
    } else if (data.password.length < 6 ) {
        errors.password = "password need to be 6 character or more"
    } else {
        delete errors.password
    }

    
    if (type === "signUp") {
        if (!data.name.trim()) {
            errors.name = "username requird"
        } else {
            delete errors.name
        }
        if (!data.confirmPassword) {
            errors.confirmPassword = "confirmPassword required"
        }else if (data.confirmPassword !== data.password) {
            errors.confirmPassword = "possword do not match"
        } else {
            delete errors.confirmPassword
        }
    
        if (data.isAccepted) {
            delete errors.isAccepted
        } else {
            errors.isAccepted = "Accept our regulations"
        }
        
    }

return errors;
}