import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Validate } from './validate';
import { notify } from './toastify';
import "./SignUp.css";

const SignUp = () => {

    const [data, setData] = useState ({
        name: "",
        email : "",
        password: "",
        confirmPassword: "",
        isAccepted: false,
    })

    const [errors, setErrors] = useState ({})
    const [touched,setTouched] = useState({});

    useEffect ( () => {
        setErrors (Validate (data,"signUp"))
    }, [data,touched])

    const focusHandler = event => {
        setTouched ({...touched, [event.target.name] : true})
    }

    const changeHandler = event =>{
        if (event.target.name === "isAccepted") {
            setData({...data, [event.target.name] : event.target.checked})
        } else {
            setData({...data, [event.target.name] : event.target.value})
        }
    }

    const submitHandler = event => {
        event.preventDefault();
        if (!Object.keys(errors).length) {
            notify ("you signd up successfuly","success")
        } else {
            notify ("Invalid data!", "error")
            setTouched ({
                name: true,
                email: true,
                password: true,
                confirmPassword: true,
                isAccepted: true
            })
        }
    }

    return (
        <div className='container'>
            <form onSubmit={submitHandler}>
                <h1 className='header'>Sign Up</h1>
                <div className= "formField">
                    <label>Name</label>
                    <input className={(errors.name && touched.name) ? "uncompleted" : "formInput"}
                        type="text" 
                        name="name" 
                        value={data.name} 
                        onChange={changeHandler}
                        onFocus= {focusHandler} />
                    {errors.name && touched.name && <span>{errors.name}</span>}
                </div>
                <div className= "formField">
                    <label>Email</label>
                    <input className={(errors.email && touched.email) ? "uncompleted" : "formInput"}
                        type="text" 
                        name='email' 
                        value={data.email} 
                        onChange={changeHandler}
                        onFocus= {focusHandler}/>
                    {errors.email && touched.email && <span>{errors.email}</span>}
                </div>
                <div className= "formField">
                    <label>Password</label>
                    <input className={(errors.password && touched.password) ? "uncompleted" : "formInput"}
                        type="password" 
                        name='password' 
                        value={data.password} 
                        onChange={changeHandler}
                        onFocus= {focusHandler}/>
                    {errors.password && touched.password && <span>{errors.password}</span>}
                </div>
                <div className= "formField">
                    <label>Confirm Password</label>
                    <input className={(errors.confirmPassword && touched.confirmPassword) ? "uncompleted" : "formInput"}
                        type="password" 
                        name='confirmPassword' 
                        value={data.confirmPassword} 
                        onChange={changeHandler}
                        onFocus= {focusHandler}/>
                    {errors.confirmPassword && touched.confirmPassword && <span>{errors.confirmPassword}</span>}
                </div>
                <div className= "formField">
                    <div className='checkboxContainer'>
                        <label>I accept term of privacy policy</label>
                        <input className={(errors.isAccepted && touched.isAccepted) ? "uncompleted" : "formInput"}
                            type="checkbox" 
                            name='isAccepted' 
                            value={data.isAccepted} 
                            onChange={changeHandler}
                            onFocus= {focusHandler}/>
                    </div>
                        {errors.isAccepted && touched.isAccepted && <span>{errors.isAccepted}</span>}
                </div>
                <div className= "formButton">
                    <Link to="/login">Login</Link>
                    <button type='submit'>signup</button>
                </div>
            </form>
            <ToastContainer />
        </div>
    );
};

export default SignUp;