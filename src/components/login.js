import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Validate } from './validate';
import { notify } from './toastify';
import "./SignUp.scss";

const Login = () => {

    const [data, setData] = useState ({
        email : "",
        password: "",
    })

    const [errors, setErrors] = useState ({})
    const [touched,setTouched] = useState({});

    useEffect ( () => {
        setErrors (Validate (data,"login"))
    }, [data,touched])

    const focusHandler = event => {
        setTouched ({...touched, [event.target.name] : true})
    }

    const changeHandler = event =>{
        if (event.target.name === "isAccepted") {
            setData({...data, [event.target.name] : event.target.checked})
        } else {
            setData({...data, [event.target.name] : event.target.value})
        }
    }

    const submitHandler = event => {
        event.preventDefault();
        if (!Object.keys(errors).length) {
            notify ("you Loged in successfuly","success")
        } else {
            notify ("Invalid data!", "error")
            setTouched ({
                name: true,
                email: true,
                password: true,
                confirmPassword: true,
                isAccepted: true
            })
        }
    }

    return (
        <div className='container'>
            <form onSubmit={submitHandler}>
                <h1 className='header'>Login</h1>
                <div className= "formField">
                    <label>Email</label>
                    <input className={(errors.name && touched.name) ? "uncompleted" : "formInput"}
                        type="text" 
                        name='email' 
                        value={data.email} 
                        onChange={changeHandler}
                        onFocus= {focusHandler}/>
                    {errors.email && touched.email && <span>{errors.email}</span>}
                </div>
                <div className= "formField">
                    <label>Password</label>
                    <input className={(errors.email && touched.email) ? "uncompleted" : "formInput"}
                        type="password" 
                        name='password' 
                        value={data.password} 
                        onChange={changeHandler}
                        onFocus= {focusHandler}/>
                    {errors.password && touched.password && <span>{errors.password}</span>}
                </div>
                <div className= "formButton">
                    <Link to='/signUp'>sign up</Link>
                    <button type='submit'>Login</button>
                </div>
            </form>
            <ToastContainer />
        </div>
    );
};

export default Login;