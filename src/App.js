import React from 'react';
import {Route, Routes, Navigate}  from "react-router-dom";
import './App.css';
import Login from './components/login';
import SignUp from './components/signUp';

function App() {
  return (
    <div className="App">
      <Routes>
      <Route path="/login" element={<Login />} />
      <Route path="/signUp" element={<SignUp />} />
      <Route path="/" element={<Navigate to="/signUp"/>} />
      </Routes>
    </div>
  );
}

export default App;
